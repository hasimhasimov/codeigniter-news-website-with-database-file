<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'anasayfa';
$route['anasayfa/(:num)'] = 'anasayfa';
$route['test'] = 'anasayfa/test';
//$route['detay/(:any)'] = 'anasayfa/detay/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

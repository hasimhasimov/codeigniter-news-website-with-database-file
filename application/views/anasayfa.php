<!DOCTYPE html>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<?php $this->load->view("front/includes/head.php"); ?>
<body class="home page-template-default page page-id-1129">
<div class="background-cover"></div>
<div id="wrap" class="grid_1200 fixed-enabled ">
<div class="wrap-content">
<div class="clearfix"></div>
<div class="clearfix"></div>
<?php $this->load->view("front/includes/header.php"); ?>
<div class="main-header-footer">
<div class="clearfix"></div>
<div class="main-sections">
<div id="sections-id-1" style='' class="sections-content sections-builder sections sections-full-width">
<div class="container">
<div class="row">
<div class="with-sidebar-container sidebar-container-builder">
<div class="main-container col-md-12">
<div class="row">
<div class="col-md-12">
<!-- Slide show -->
<?php $this->load->view("front/includes/slideshow.php"); ?>
<div class="clearfix"></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div id="sections-id-3" style='' class="sections-content sections-builder sections sections-right-sidebar">
<div class="container">
<div class="row">
<div class="with-sidebar-container sidebar-container-builder">
<div class="main-container col-md-8">
<div class="row">
	<?php foreach($cats as $cats){ ?>
<div class="col-md-12">
<div class="box-border">
<div class="post-title post-title-news">
<h3><a href='<?php echo base_url('anasayfa/category/'.$cats['selflink']); ?>'><?php echo $cats['title']; ?></a></h3></div>
<div class="clearfix"></div>

<div class="box-news box-news-1 box-news-sidebar">
<div class="row">
<ul>
	<?php $sonhaber=habersoncek($cats['idn']);
	foreach($sonhaber as $son){ ?>
<li class="col-md-6 col-sm-6 col-xs-6 col-mo-100">
<div class="box-news-big">
<div class="box-news-img">
<img alt='' width='409' height='260' src='<?php echo base_url($son['tmb']); ?>'>
<div class="box-news-overlay"><a href="<?php echo base_url('anasayfa/detay/'.$son['selflink']); ?>" title="" rel="bookmark"></a></div>
</div>

<div class="box-news-content">
<div class="box-news-title">
	<a href="<?php echo base_url('anasayfa/detay/'.$son['selflink']); ?>" title="" rel="bookmark"><?php echo $son['title']; ?></a>
</div>
<div class="box-news-meta">
<time class="box-news-meta-date"><i class="fa fa-calendar"></i><?php echo $son['date']; ?></time>
<a class="box-news-meta-comment"><i class="fa fa-comments-o"></i><?php echo yorumcek($son['idn']); ?></a>
<div class="box-news-meta-view"><i class="fa fa-eye"></i><?php echo $son['click']; ?></div>
</div>
<!-- End box-news-meta -->
<div class="box-news-desc">
<p><?php echo word_limiter($son['content'], 25); ?></p>
</div>
<div class="box-news-more"><a href="<?php echo base_url('anasayfa/detay/'.$son['selflink']); ?>" title="" rel="bookmark">Devamı</a></div>
</div>
<!-- End box-news-content -->
<div class="clearfix"></div>
</div>
<!-- End box-news-big -->
</li>
<?php } ?>
<?php $haber4 = haber4cek($cats['idn']);
foreach($haber4 as $h4){ ?>
<!-- End col-* -->
<li class="col-md-6 col-sm-6 col-xs-6 col-mo-100">
<div class="box-news-small">
<div class="box-news-img">
<img alt='' width='94' height='73' src='<?php echo base_url($h4['mini']); ?>'>
<div class="box-news-overlay"><a href="<?php echo base_url('anasayfa/detay/'.$h4['selflink']); ?>" title="" rel="bookmark"></a></div>
</div>
<!-- End box-news-img -->
<div class="box-news-content">
<div class="box-news-title"><a href="<?php echo base_url('anasayfa/detay/'.$h4['selflink']); ?>" title="" rel="bookmark"><?php echo $h4['title']; ?></a></div>
<div class="box-news-meta">
<time class="box-news-meta-date"><i class="fa fa-calendar"></i><?php echo  $h4['date']; ?></time>
<a class="box-news-meta-comment" href="#"><i class="fa fa-comments-o"></i><?php echo yorumcek($h4['idn']); ?></a>
<div class="box-news-meta-view"><i class="fa fa-eye"></i><?php echo $h4['click']; ?></div>
</div>
<!-- End box-news-meta -->
</div>
<!-- End box-news-content -->
<div class="clearfix"></div>
</div>
<!-- End box-news-small -->
</li>
<!-- End col-* -->
<?php } ?>
</ul>
<!-- End ul -->
</div>
<!-- End row -->
</div>
<!-- End box-news -->
<div class="clearfix"></div>
</div>
<!-- End box-border -->
</div>
<?php } ?>
<div class='clearfix'></div>
<?php $this->load->view("front/includes/sondakikahaber.php"); ?>
<?php $this->load->view("front/includes/althaber.php"); ?>
<div class="not_duplicate_array" data-not_duplicate=""></div>
</div>
</div>
<div class="first-sidebar sidebar-col col-md-4 sticky-sidebar">
<aside class="sidebar">
<?php $this->load->view("front/includes/arama.php"); ?>
<?php $this->load->view("front/includes/socialmedia.php"); ?>
<?php $this->load->view("front/includes/tablar.php"); ?>
<?php $this->load->view("front/includes/sonhaber.php"); ?>
<?php $this->load->view("front/includes/koseyazari.php"); ?>
</aside>
</div>
<div class="clearfix"></div>
</div>

</div>
</div>
<?php $this->load->view("front/includes/footer.php"); ?>
</body>
</html>

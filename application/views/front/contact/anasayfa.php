<!DOCTYPE html>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>HNews | Əlaqə</title>
<!--========style.php============== -->
<?php $this->load->view("front/includes/style.php"); ?>
</head>
<body class="home page-template-default page page-id-1129">
<div class="background-cover"></div>
<div id="wrap" class="grid_1200 fixed-enabled ">
<div class="wrap-content">
<div class="clearfix"></div>
<div class="clearfix"></div>
<?php $this->load->view("front/includes/header.php"); ?>
<div class="main-header-footer">
	<div class="breadcrumbs-main main-block breadcrumbs-main-3">
		<div class="container">
			<div class="breadcrumbs">
				<div class="crumbs">
					<a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Ana Səhifə</a>
					<span class="crumbs-span">&raquo;</span>
					<span class="current">Əlaqə</span>
				</div>
			</div>
		</div>
	</div>
<div class="clearfix"></div>
<div class="main-sections">
<div id="sections-id-3" style='' class="sections-content sections-builder sections sections-right-sidebar">
<div class="container">
<div class="row">
<div class="with-sidebar-container sidebar-container-builder">
<div class="main-container col-md-8">
<div class="clearfix"></div>

<div class="v-post post-contact-us post-no-border">
	<div class="post-inner">
		<div class="post-title"><h3>HNews</h3></div>
		<div class="row">
			<div class="col-md-12">
				<div class="screen-reader-response"></div>
				<h3>ƏLAQƏ</h3>
				<?php $ab=siteayarlari(); ?>
				<p>Ünvan: <?php echo $ab['adress']; ?> </p>
				<p>Telefon: <?php echo $ab['phone']; ?> </p>
				<p>Email: <?php echo $ab['mail']; ?></p>
				<form method="POST" action="">
					<label>Ad Soyad</label>
					<input type="text" name="name" class="form-control">
				</form>
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3039.4320036867703!2d49.902857015045576!3d40.377117079369704!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40307cb8ddb14155%3A0x5d5a3ea4f4a1992b!2sNobel+Ave%2C+Baku%2C+Azerbaycan!5e0!3m2!1str!2s!4v1530168306383"  height="450" width="750" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
</div>
<div class="first-sidebar sidebar-col col-md-4 sticky-sidebar">
<aside class="sidebar">
<?php $this->load->view("front/includes/arama.php"); ?>
<?php $this->load->view("front/includes/socialmedia.php"); ?>
<?php $this->load->view("front/includes/tablar.php"); ?>
<?php $this->load->view("front/includes/sonhaber.php"); ?>
<?php $this->load->view("front/includes/koseyazari.php"); ?>
</aside>
</div>
<div class="clearfix"></div>

</div>

</div>
</div>
<?php $this->load->view("front/includes/footer.php"); ?>
</body>
</html>

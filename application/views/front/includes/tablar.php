<div class='widget tabs-warp widget-tabs'>
<div class="widget-title widget-title-tabs">
<ul class="tabs tabs359">
<li class="tab"><a href="#">Most Read</a></li>
<li class="tab"><a href="#">Comments</a></li>
<li class="clearfix"></li>
</ul>
</div>
<div class="widget-wrap">
<div class='tab-inner-warp tab-inner-warp359'>
<ul class='widget-posts'>
<?php foreach($mnews as $mnews){ ?>
<li class="widget-posts-slideshow">
<div class="box-news-small">
<div class="box-news-img">
<img alt='Gallery Post full width with meta' width='94' height='73' src='<?php echo base_url($mnews['mini']); ?>'>
<div class="box-news-overlay"><a href="<?php echo base_url('anasayfa/detay/'.$mnews['selflink']); ?>" title="" rel="bookmark"></a></div>
</div>
<div class="box-news-content">
<div class="box-news-title"><a href="<?php echo base_url('anasayfa/detay/'.$mnews['selflink']); ?>" title="" rel="bookmark"><?php echo $mnews['title']; ?></a></div>
<div class="box-news-meta">
<time class="box-news-meta-date"><i class="fa fa-calendar"></i><?php echo $mnews['date']; ?></time>
<a class="box-news-meta-comment"><i class="fa fa-comments-o"></i><?php echo yorumcek($mnews['idn']); ?></a>
</div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
</li>
<?php } ?>


</ul>
</div>
<div class='tab-inner-warp tab-inner-warp359'>
<div class='widget-comments'>
<ol class='commentlist clearfix'>
	<?php $comment=get_comment();
	foreach($comment as $com){ ?>
<li class="comment">
<div class="comment-body">
<div class="avatar">
<a href="#">
<img alt='avatar' src='<?php echo base_url('assets/front/'); ?>images/avatar.png'>
</a>
</div>
<div class="comment-text">
<div class="author clearfix">
<div class="comment-meta">
<span class="comment-author fn"><a href='#' target='_blank'><?php echo $com['name']; ?></a></span>
<div class="clearfix"></div>
</div>
</div>
<div class="text">
<p><a href="#"><?php echo word_limiter($com['comment'], 15); ?></a></p>
</div>
</div>
</div>
</li>
<?php } ?>

</ol>
</div>
</div>
</div>
</div>
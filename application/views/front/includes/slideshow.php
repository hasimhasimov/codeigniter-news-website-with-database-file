<div class="box-slideshow box-slideshow-full slide-show-full slideshow-style-3">
<ul>
<?php $slider = slidernews();
foreach($slider as $s){
?>
<li class="slider-item">
<div class="box-slideshow-main box-slideshow-big" style="width: 100%;">
<div class="box-slideshow-img">
<img alt='' style="width: 100%;" src='<?php echo base_url($s['resim']); ?>'> </div>
<div class="slideshow-overlay"></div>
<div class="box-slideshow-content">
<div class="box-slideshow-outer">
<div class="box-slideshow-inner">
<a class="box-news-overlay-3" href="<?php echo base_url('anasayfa/detay/'.$s['selflink']); ?>" title="" rel="bookmark"></a>
<span class="slide-category">
<a title=""><?php $cat=get_cat_name($s['cat_id']); 
foreach ($cat as $cat) {
	echo $cat['title'];
}
?></a>
</span>
<span class="slide-date"><i class="fa fa-calendar"></i><?php echo $s['date']; ?></span>
<div class="clearfix"></div>
<a href="#" title="" rel="bookmark"><?php echo $s['title']; ?> </a>
</div>
</div>
</div>
</div>
</li>
<?php } ?>
</ul>
</div>
<div class="hidden-header">
<div class="bottom-header main-block bottom-header-2">
<div class="container">
<nav class="navigation" rel="navigation_main">
<div class="header-menu">
<ul id="menu-header" class="">
<li class="menu-item menu-item-type-custom"><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i> HOME</a></li>
</li>
<?php foreach($cats as $cats){ ?>
<li class="menu-item menu-item-type-custom"><a href="<?php echo base_url().'anasayfa/category/'.$cats['selflink'] ?>"><i class="fa fa-<?php echo $cats['icon']; ?>"></i> <?php echo $cats['title']; ?></a></li>
<?php } ?>
</div>
</nav>
<nav class="navigation_main navigation_mobile navigation_mobile_main">
<div class="navigation_mobile_click">See...</div>
<ul></ul>
</nav>
</div>
</div>
<div class="clearfix"></div>
</div>

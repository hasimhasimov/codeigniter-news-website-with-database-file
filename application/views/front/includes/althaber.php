<div class="col-md-12">
<div class="box-border">
<div class='all-blogs'>

<?php foreach($rnews as $rnews){ ?>
<article class="v-post clearfix article-title-meta-image post-style-6 animation post--content image_post post-no-border post-1912 post type-post status-publish format-standard has-post-thumbnail hentry category-business" data-animate="fadeInUp" itemscope=""
itemtype="http://schema.org/Article">
<div class="post-img post-img-9">
<a href="#" title="" rel="bookmark" class='post-img-link'>
<img alt='' width='280' height='230' src='<?php echo base_url($rnews['tmb']); ?>'>
</a>
</div>
<div class='post-inner-6'>
<h1 class="post-head-title">
<a itemprop="url" href="#" title="" rel="bookmark">
<?php echo $rnews['title']; ?></a>
</h1>
<div class="post-meta">
<div class="post-meta-date"><i class="fa fa-calendar"></i><?php echo $rnews['date']; ?></div>
<div class="post-meta-category"><i class="fa fa-folder-o"></i><a href="#" rel="category tag"><?php $cat=get_cat_name($rnews['cat_id']); 
foreach ($cat as $cat) {
	echo $cat['title'];
}
?></a></div>
<div class="post-meta-comment"><i class="fa fa-comments-o"></i><?php echo yorumcek($rnews['idn']); ?> Comments</div>
</div>
<div class="clearfix"></div>
<div class="post-inner">
<p><?php echo word_limiter($rnews['content'],35); ?></35></p>
<div class="post-more"><a class="button-default medium" href="#" title="" rel="bookmark"><i class="fa fa-long-arrow-right"></i>Continue</a></div>
<div class="clearfix"></div>
</div>
</div>
</article>
<?php } ?>

</div>
</div>
<?php echo $linkler; ?>
</div>
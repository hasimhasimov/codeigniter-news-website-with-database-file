<footer id="main-footer">
<div class="top-footer top-footer-2" style=" padding-top:60px; padding-bottom:60px;">
<div class="container">
<div class="row">
<div class="col-md-4">
<div id="about_me-widget-4" class="widget widget-about">
<div class="widget-title">
<h3>About Us</h3></div>
<div class="clearfix"></div>
<div class="widget-wrap">
<div class="about-widget">
<div class="widget-about-img">
<a href='<?php echo base_url(); ?>' data-rel='prettyPhoto'><img alt='' src='<?php echo base_url("assets/front/") ?>images/trlogo.png'></a> </div>
<p style='margin-top: 20px'>
	<?php echo $info['info']; ?><br/>
	<?php echo $info['adress']; ?><br/>
	<?php echo $info['phone']; ?>
</p>
</div>
</div>
</div>
</div>
<div class="col-md-4">
<div id="widget_posts-7" class="widget widget-posts">
<div class="widget-title">
<h3>Latest News</h3></div>
<div class="clearfix"></div>
<div class="widget-wrap">
<ul class='widget-posts'>
<?php foreach($lnews as $lnews){ ?>
<li class="widget-posts-image">
<div class="box-news-small">
<div class="box-news-img">
<img alt='' width='94' height='73' src='<?php echo base_url($lnews['mini']); ?>'>
<div class="box-news-overlay"><a href="<?php echo base_url('anasayfa/detay/'.$lnews['selflink']); ?>" title="" rel="bookmark"></a></div>
</div>
<div class="box-news-content">
<div class="box-news-title"><a href="<?php echo base_url('anasayfa/detay/'.$lnews['selflink']); ?>" title="" rel="bookmark"><?php echo $lnews['title']; ?></a></div>
<div class="box-news-meta">
<time class="box-news-meta-date"><i class="fa fa-calendar"></i><?php echo $lnews['date']; ?></time>
<a class="box-news-meta-comment"><i class="fa fa-comments-o"></i><?php echo yorumcek($lnews['idn']); ?></a>
</div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
</li>
<?php } ?>


</ul>
</div>
</div>
</div>


<div class="col-md-4">
<div id="widget_posts-7" class="widget widget-posts">
<div class="widget-title">
<h3>Most Read News</h3></div>
<div class="clearfix"></div>
<div class="widget-wrap">
<ul class='widget-posts'>
<?php foreach($mnews as $mnews){ ?>
<li class="widget-posts-image">
<div class="box-news-small">
<div class="box-news-img">
<img alt='' width='94' height='73' src='<?php echo base_url($mnews['mini']); ?>'>
<div class="box-news-overlay"><a href="<?php echo base_url('anasayfa/detay/'.$mnews['selflink']); ?>" title="" rel="bookmark"></a></div>
</div>
<div class="box-news-content">
<div class="box-news-title"><a href="<?php echo base_url('anasayfa/detay/'.$mnews['selflink']); ?>" title="" rel="bookmark"><?php echo $mnews['title']; ?></a></div>
<div class="box-news-meta">
<time class="box-news-meta-date"><i class="fa fa-calendar"></i><?php echo $mnews['date']; ?></time>
<a class="box-news-meta-comment"><i class="fa fa-comments-o"></i><?php echo yorumcek($mnews['idn']); ?></a>
</div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
</li>
<?php } ?>



</ul>
</div>
</div>
</div>

</div>
</div>
</div>

<div id="footer" class='main-block footer-normal footer-social-copyrights footer-have-top footer-2'>
<div class="container">
<div class="social-ul">
<ul>
<?php $sm = get_social_menu();
	foreach($sm as $s){ ?>
<li class="social-<?php echo $s['title'] ?>"><a class="tooltip-n" href="<?php echo $s['url'] ?>" target='_blank'><i class="fa fa-<?php echo $s['title'] ?>"></i></a></li>
<?php } ?>
</ul>
</div>

<div class="copyrights">Copyright <?php echo date('Y'); ?> | Developed by <a href=http://www.facebook.com/hasimovhasim target=_blank>Hasim Hasimov</a></div>
</div>
</div>
</footer>
<div class="clearfix"></div>
</div>
</div>
<div class="go-up"><i class="fa fa-chevron-up"></i></div>



<script type='text/javascript' src='<?php echo base_url('assets/front/'); ?>js/jquery/jqueryb8ff.js?ver=1.12.4'></script>
<script type='text/javascript' src='<?php echo base_url('assets/front/'); ?>js/scripts8a54.js?ver=1.0.0'></script>
<script type='text/javascript' src='<?php echo base_url('assets/front/'); ?>js/custom8a54.js?ver=1.0.0'></script>
<script type='text/javascript' src='<?php echo base_url('assets/front/'); ?>js/custom.js?ver=1.0.0'></script>


<div id="widget_posts-5" class="widget widget-posts">
<div class="widget-title">
<h3>Latest News</h3></div>
<div class="clearfix"></div>
<div class="widget-wrap">
<ul class='widget-posts'>
<?php foreach($lnews as $lnews){ ?>
<li class="widget-posts-image">
<div class="box-news-small">
<div class="box-news-img">
<img alt='' width='94' height='73' src='<?php echo base_url($lnews['mini']); ?>'>
<div class="box-news-overlay"><a href="<?php echo base_url('anasayfa/detay/'.$lnews['selflink']); ?>" title="" rel="bookmark"></a></div>
</div>
<div class="box-news-content">
<div class="box-news-title"><a href="<?php echo base_url('anasayfa/detay/'.$lnews['selflink']); ?>" title="" rel="bookmark"><?php echo $lnews['title']; ?></a></div>
<div class="box-news-meta">
<time class="box-news-meta-date"><i class="fa fa-calendar"></i><?php echo $lnews['date']; ?></time>
<a class="box-news-meta-comment"><i class="fa fa-comments-o"></i><?php echo yorumcek($lnews['idn']); ?></a>
</div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
</li>
<?php } ?>





</ul>
</div>
</div>
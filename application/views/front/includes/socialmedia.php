<div id="widget_counter-4" class="widget widget-statistics widget-margin-custom widget-margin-20">
<div class="widget-title">
<h3>Social Media</h3></div>
<div class="clearfix"></div>
<div class="widget-wrap">
<div class="social-ul">
<ul class=" social-background">
		<?php $sm = get_social_menu();
	foreach($sm as $s){ ?>
<li class="social-<?php echo $s['title']; ?>">
<a href="<?php echo $s['url']; ?>" target="_blank"><i class="fa fa-<?php echo $s['title']; ?>"></i></a>
</li>
<?php } ?>
</ul>
</div>
<div class="clearfix"></div>
</div>
</div>
<div id="widget_posts-5" class="widget widget-posts">
<div class="widget-title">
<h3>Authors</h3></div>
<div class="clearfix"></div>
<div class="widget-wrap">
<ul class="widget-posts">

<?php foreach($yazar as $yazar){ ?>
<li class="widget-posts-image">
<div class="box-news-small">
<div class="box-news-img">
<img alt="" width="94" height="73" src="<?php echo base_url($yazar['resim']); ?>">
<div class="box-news-overlay"><a href="#" title="" rel="bookmark"></a></div>
</div>
<div class="box-news-content">
<div class="box-news-title"><a href="#" title="" rel="bookmark"><?php echo $yazar['name']; ?> </a></div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
</li>
<?php } ?>

<div class="clearfix"></div>
</ul></div>
</div>
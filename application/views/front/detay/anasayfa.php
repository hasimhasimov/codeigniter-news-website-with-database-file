<!DOCTYPE html>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>HNews | <?php echo $read['title']; ?></title>
<!--========style.php============== -->
<?php $this->load->view("front/includes/style.php"); ?>
</head>
<body class="home page-template-default page page-id-1129">
<div class="background-cover"></div>
<div id="wrap" class="grid_1200 fixed-enabled ">
<div class="wrap-content">
<div class="clearfix"></div>
<div class="clearfix"></div>
<?php $this->load->view("front/includes/header.php"); ?>
<div class="main-header-footer">
	<div class="breadcrumbs-main main-block breadcrumbs-main-3">
		<div class="container">
			<div class="breadcrumbs">
				<div class="crumbs">
					<a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Ana Səhifə</a>
					<span class="crumbs-span">&raquo;</span>
                     <?php $cat=get_cat_name($read['cat_id']); 
                        foreach ($cat as $cat) {
	                        ?>
	                        <a href="<?php echo base_url('anasayfa/category/'.$cat['selflink']); ?>"><i class="fa fa-folder"></i> <?php echo $cat['title']; ?></a>
	                        <?php
                        }
                    ?>
					<span class="crumbs-span">&raquo;</span>
					<span class="current"><?php echo $read['title']; ?></span>
				</div>
			</div>
		</div>
	</div>
<div class="clearfix"></div>
<div class="main-sections">
<div id="sections-id-3" style='' class="sections-content sections-builder sections sections-right-sidebar">
<div class="container">
<div class="row">
<div class="with-sidebar-container sidebar-container-builder">
<div class="main-container col-md-8">
<div class="clearfix"></div>

<article class="v-post clearfix article-title-meta-image post-style-12 animation post--content image_post post-no-border post-1912 post type-post status-publish format-standard has-post-thumbnail hentry category-business" data-animate="fadeInUp" itemscope=""
itemtype="http://schema.org/Article">
<div >
		<h1 class="post-head-title">
<a itemprop="url" href="#" title="" rel="bookmark">
<?php echo $read['title']; ?></a>
</h1>
<div class="post-meta">
<div class="post-meta-date"><i class="fa fa-calendar"></i><?php echo $read['date']; ?></div>
<div class="post-meta-category"><i class="fa fa-folder-o"></i><a href="#" rel="category tag"><?php $cat=get_cat_name($read['cat_id']); 
foreach ($cat as $cat) {
	echo $cat['title'];
}
?></a></div>
<div class="post-meta-comment"><i class="fa fa-comments-o"></i><?php echo yorumcek($read['idn']); ?> Rəy</div>
<div class="post-meta-comment"><i class="fa fa-eye"></i><?php echo $read['click']; ?> Oxunma</div>
</div>
</div>
<div class="post-img post-img-12">
<a href="#" title="" rel="bookmark" class='post-img-link'>
<img alt='' style="width:100%;" src='<?php echo base_url($read['resim']); ?>'>
</a>
</div>
<div class='post-inner-12'>
<div class="clearfix"></div>
<div class="post-inner">
<p><?php echo $read['content']; ?></35></p>
<div class="clearfix"></div>
</div>
</div>
<div class="post-share">
<span class="share-text">Paylaş:</span>
<ul class="flat-social">
<li><a href="" class="social-facebook" rel="external" target="_blank"><i class="fa fa-facebook"></i> <span>Facebook</span></a></li>
<li><a href="" class="social-twitter" rel="external" target="_blank"><i class="fa fa-twitter"></i> <span>Twitter</span></a></li>
<li><a href="" class="social-google-plus" rel="external" target="_blank"><i class="fa fa-google-plus"></i> <span>Google +</span></a></li>
</ul>
<div class="clear"></div>
</div>
</article>
<?php if($read['comment']==1){ 
	 echo   $this->session->flashdata('error');
	?>
<div class="post-title"><h3>Rəylər</h3></div><div class="clearfix"></div>
<ol class="commentlist clearfix">
<?php $com = yorumsay($read['idn']);
      foreach($com as $c){
?>
	<li class="comment even thread-even depth-1 single-comment" id="li-comment-73">
		<div id="comment-73" class="comment-body">
			<div class="avatar">
				<img src="<?php echo base_url('assets/front/images/avatar.png'); ?>" srcset="<?php echo base_url('assets/front/images/avatar.png'); ?>" class="avatar avatar-70 photo" height="70" width="70">
			</div>
			<div class="comment-text">
				<div class="author clearfix">
					<div class="comment-meta">
						<span class="comment-author fn"><?php echo $c['name']; ?></span>
						<div class="date"><a><i class="fa fa-calendar"></i><time><?php echo $c['date']; ?></time></a></div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="text">
					<p><?php echo $c['comment']; ?></p>
				</div>
			</div>
		</div>
	</li>
	<?php } ?>
</ol>
<div class="post-inner">
	<div class="post-title"><h3>Rəy Bildirin</h3></div><div class="clearfix"></div>
	<div class="comment-form">
		<form  action="<?php echo base_url('anasayfa/comments'); ?>" method="POST" id="commentform">
			<div class="form-input">
				<input type="text" name="name" required placeholder="Ad Soyad">
			</div>
			<div class="form-input form-input-last">
				<input type="email" name="email" required placeholder="Email">
			</div>
			<div class="form-input form-textarea">
				<textarea id="comment" name="comment" required placeholder="Rəyiniz"></textarea>
			</div>
			<input type="submit" name="submit" id="submit" value="Rəy Yaz" class="button-default">
			<input type="hidden" name="newsid" value="<?php echo $read['idn']; ?>">
			<input type="hidden" name="newsself" value="<?php echo $read['selflink']; ?>">
			<input type="hidden" name="newscat" value="<?php echo $read['cat_id']; ?>">
			<input type="hidden" name="ip" value="<?php echo GetIP(); ?>">
			<div class="clearfix"></div>
		</form>
	</div>
</div>
<?php }?>

<?php $this->load->view('front/includes/sondakikahaber.php'); ?>

</div>
<div class="first-sidebar sidebar-col col-md-4 sticky-sidebar">
<aside class="sidebar">
<?php $this->load->view("front/includes/arama.php"); ?>
<?php $this->load->view("front/includes/socialmedia.php"); ?>
<?php $this->load->view("front/includes/tablar.php"); ?>
<?php $this->load->view("front/includes/sonhaber.php"); ?>
<?php $this->load->view("front/includes/koseyazari.php"); ?>
</aside>
</div>
<div class="clearfix"></div>

</div>

</div>
</div>
<?php $this->load->view("front/includes/footer.php"); ?>
</body>
</html>

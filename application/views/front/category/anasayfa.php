<!DOCTYPE html>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>HNews | Category</title>
<!--========style.php============== -->
<?php $this->load->view("front/includes/style.php"); ?>
</head>
<body class="home page-template-default page page-id-1129">
<div class="background-cover"></div>
<div id="wrap" class="grid_1200 fixed-enabled ">
<div class="wrap-content">
<div class="clearfix"></div>
<div class="clearfix"></div>
<?php $this->load->view("front/includes/header.php"); ?>
<div class="main-header-footer">
	<div class="breadcrumbs-main main-block breadcrumbs-main-3">
		<div class="container">
			<div class="breadcrumbs">
				<div class="crumbs">
					<a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Ana Səhifə</a>
					<span class="crumbs-span">&raquo;</span>
					<span class="current"><?php echo $ccat['title']; ?></span>
				</div>
			</div>
		</div>
	</div>
<div class="clearfix"></div>
<div class="main-sections">
<div id="sections-id-3" style='' class="sections-content sections-builder sections sections-right-sidebar">
<div class="container">
<div class="row">
<div class="with-sidebar-container sidebar-container-builder">
<div class="main-container col-md-8">
<div class="post-title">
<h3>Kateqoriya: <?php echo $ccat['title']; ?></h3>
</div>
<div class="clearfix"></div>
<div class="all-blog">
<?php foreach($rnews as $rnews){ 
	?>
<article class="v-post clearfix article-title-meta-image post-style-6 animation post--content image_post post-no-border post-1912 post type-post status-publish format-standard has-post-thumbnail hentry category-business" data-animate="fadeInUp" itemscope=""
itemtype="http://schema.org/Article">
<div class="post-img post-img-9">
<a href="<?php echo base_url('anasayfa/detay/'.$rnews['selflink']); ?>" title="" rel="bookmark" class='post-img-link'>
<img alt='' width='280' height='230' src='<?php echo base_url($rnews['tmb']); ?>'>
</a>
</div>
<div class='post-inner-6'>
<h1 class="post-head-title">
<a itemprop="url" href="<?php echo base_url('anasayfa/detay/'.$rnews['selflink']); ?>" title="" rel="bookmark">
<?php echo $rnews['title']; ?></a>
</h1>
<div class="post-meta">
<div class="post-meta-date"><i class="fa fa-calendar"></i><?php echo $rnews['date']; ?></div>
<div class="post-meta-category"><i class="fa fa-folder-o"></i><a href="<?php echo base_url('anasayfa/category/'.$ccat['selflink']); ?>" rel="category tag"><?php $cat=get_cat_name($rnews['cat_id']); 
foreach ($cat as $cat) {
	echo $cat['title'];
}
?></a></div>
<div class="post-meta-comment"><i class="fa fa-comments-o"></i><?php echo yorumcek($rnews['idn']); ?> Rəy</div>
<div class="post-meta-comment"><i class="fa fa-eye"></i><?php echo $rnews['click']; ?> Oxunma</div>
</div>
<div class="clearfix"></div>
<div class="post-inner">
<p><?php echo word_limiter($rnews['content'],35); ?></35></p>
<div class="post-more"><a class="button-default medium" href="<?php echo base_url('anasayfa/detay/'.$rnews['selflink']); ?>" title="" rel="bookmark"><i class="fa fa-long-arrow-right"></i>Devamı</a></div>
<div class="clearfix"></div>
</div>
</div>
</article>
<?php } ?>
<?php echo $linkler; ?>
</div>
</div>
<div class="first-sidebar sidebar-col col-md-4 sticky-sidebar">
<aside class="sidebar">
<?php $this->load->view("front/includes/arama.php"); ?>
<?php $this->load->view("front/includes/socialmedia.php"); ?>
<?php $this->load->view("front/includes/tablar.php"); ?>
<?php $this->load->view("front/includes/sonhaber.php"); ?>
<?php $this->load->view("front/includes/koseyazari.php"); ?>
</aside>
</div>
<div class="clearfix"></div>

</div>

</div>
</div>
<?php $this->load->view("front/includes/footer.php"); ?>
</body>
</html>

<!DOCTYPE html>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>HNews | Künye</title>
<!--========style.php============== -->
<?php $this->load->view("front/includes/style.php"); ?>
</head>
<body class="home page-template-default page page-id-1129">
<div class="background-cover"></div>
<div id="wrap" class="grid_1200 fixed-enabled ">
<div class="wrap-content">
<div class="clearfix"></div>
<div class="clearfix"></div>
<?php $this->load->view("front/includes/header.php"); ?>
<div class="main-header-footer">
	<div class="breadcrumbs-main main-block breadcrumbs-main-3">
		<div class="container">
			<div class="breadcrumbs">
				<div class="crumbs">
					<a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Ana Səhifə</a>
					<span class="crumbs-span">&raquo;</span>
					<span class="current">Künye</span>
				</div>
			</div>
		</div>
	</div>
<div class="clearfix"></div>
<div class="main-sections">
<div id="sections-id-3" style='' class="sections-content sections-builder sections sections-right-sidebar">
<div class="container">
<div class="row">
<div class="with-sidebar-container sidebar-container-builder">
<div class="main-container col-md-8">
<div class="clearfix"></div>

<div class="v-post post-contact-us post-no-border">
	<div class="post-inner">
		<div class="post-title"><h3>Yayım Komandası</h3></div>
		<div class="row">
			<div class="col-md-12">
				<div class="screen-reader-response"></div>
				<h3>Künye</h3>
				<ul>
					<?php $kunye=kunyecek(); 
					foreach($kunye as $k){ ?>
					<li><?php echo $k['name'] ?> - <b><?php echo $k['gorev']; ?></b> </li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
</div>
<div class="first-sidebar sidebar-col col-md-4 sticky-sidebar">
<aside class="sidebar">
<?php $this->load->view("front/includes/arama.php"); ?>
<?php $this->load->view("front/includes/socialmedia.php"); ?>
<?php $this->load->view("front/includes/tablar.php"); ?>
<?php $this->load->view("front/includes/sonhaber.php"); ?>
<?php $this->load->view("front/includes/koseyazari.php"); ?>
</aside>
</div>
<div class="clearfix"></div>

</div>

</div>
</div>
<?php $this->load->view("front/includes/footer.php"); ?>
</body>
</html>

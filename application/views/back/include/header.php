 <header class="main-header">
    <a href="<?php echo base_url('yonetim'); ?>" class="logo">
      <span class="logo-mini"><b>H</b>N</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>H News</b>HN</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
    </nav>
  </header>

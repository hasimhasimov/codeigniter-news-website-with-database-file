<!-- CK Editor -->
<script src="<?php echo base_url("assets/back/"); ?>bower_components/ckeditor/ckeditor.js"></script>
<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/back/'); ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/back/'); ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assets/back/'); ?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/back/'); ?>bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/back/'); ?>dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/back/'); ?>dist/js/demo.js"></script>
<script src="<?php echo base_url('assets/back/'); ?>dist/js/custom.js"></script>
<script src="<?php echo base_url('assets/back/'); ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets/back/'); ?>dist/js/bootstrap-toggle.min.js"></script>
<script src="<?php echo base_url('assets/back/'); ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>

  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
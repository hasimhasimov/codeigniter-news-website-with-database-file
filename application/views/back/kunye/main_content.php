<!-- Main content -->
    <section class="content">
      <div class="row">
        <?php echo $this->session->flashdata("durum"); ?>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Kunye List</h3>
              <a href="<?php echo base_url('yonetim/kunyeekle'); ?>" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Isim Soyisim</th>
                  <th>Gorevi</th>
                  <th>Status</th>
                  <th>Operations</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($bilgi as $bilgi){ ?>
                <tr>
                  <td><?php echo $bilgi['idn']; ?></td>
                  <td><?php echo $bilgi['name']; ?></td>
                  <td><?php echo $bilgi['gorev']; ?></td>
                  <td><input
                    class="toggle_check"
                    data-toggle="toggle"
                    data-on="Active"
                    data-onstyle="success"
                    data-off="Deactive" 
                    data-offstyle="danger"
                    type="checkbox"
                    dataId="<?php echo $bilgi['idn']; ?>"
                    dataURL="<?php echo base_url("yonetim/kunyeset"); ?>"
                    <?php echo ($bilgi['status']==1) ? "checked" : ""; ?>
                  >
                  </td>
                  <td><a href="<?php echo base_url('yonetim/kunyeduzenle/'.$bilgi['idn'].''); ?>">
                    <button type="button" class="btn btn-info" name="info">Edit</button></a>
                  <a href="<?php echo base_url('yonetim/kunyesil/'.$bilgi['idn'].'/idn/kunye'); ?>">
                    <button type="button" class="btn btn-danger" name="delete">Delete</button></a></td>
                </tr>
                <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
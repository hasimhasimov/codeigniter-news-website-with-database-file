<!-- Main content -->
    <section class="content">
      <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Kunye Ekleme</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="<?php echo base_url('yonetim/kunyeekleme'); ?>" method="POST">
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Isim Soyisim</label>

                  <div class="col-sm-6">
                    <input type="text" class="form-control" placeholder="Isim Soyisim" name="name">
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Gorevi</label>

                  <div class="col-sm-6">
                    <input type="text" class="form-control" placeholder="Gorev" name="gorev">
                  </div>
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-default"><a href="<?php echo base_url("yonetim/kunye"); ?>">Cancel</a></button>
                <button type="submit" class="btn btn-primary pull-right">Add</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <div class="clearfix"></div>
    </section>
    <div class="clearfix"></div>
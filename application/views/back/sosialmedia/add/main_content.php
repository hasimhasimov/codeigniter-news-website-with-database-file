<!-- Main content -->
    <section class="content">
      <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Site Ayarlari Duzenleme</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="<?php echo base_url('yonetim/addsocialmedia'); ?>" method="POST">
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Social Media Title</label>

                  <div class="col-sm-6">
                    <input type="text" class="form-control" placeholder="Social Media Title" name="title">
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Social Media Url</label>

                  <div class="col-sm-6">
                    <input type="text" class="form-control" placeholder="Social Media Url Title" name="url">
                  </div>
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-default"><a href="<?php echo base_url("yonetim/sosialmedia"); ?>">Cancel</a></button>
                <button type="submit" class="btn btn-primary pull-right">Add</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <div class="clearfix"></div>
    </section>
    <div class="clearfix"></div>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <?php echo $this->session->flashdata("durum"); ?>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Comments</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Ad Soyad</th>
                  <th>Email</th>
                  <th>Date</th>
                  <th>Status</th>
                  <th>Operations</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($bilgi as $bilgi){ ?>
                <tr>
                  <td><?php echo $bilgi['idn']; ?></td>
                  <td><?php echo $bilgi['name']; ?></td>
                  <td><?php echo $bilgi['email']; ?></td>
                  <td><?php echo $bilgi['date']; ?></td>
                  <td> <input 
                    class="toggle_check"
                    data-toggle="toggle"
                    data-on="Active"
                    data-onstyle="success"
                    data-off="Deactive" 
                    data-offstyle="danger"
                    type="checkbox"
                    <?php echo ($bilgi['status']==1) ? "checked" : ""; ?> 
                    onchange="go(<?php echo $bilgi['idn']; ?>, '<?php echo base_url("yonetim/yorumset"); ?>','<?php echo ($bilgi['status']==1) ? "false" : "true"; ?>'  );">
                  </td>
                  <td><a href="<?php echo base_url('yonetim/yorumedit/'.$bilgi['idn'].''); ?>">
                    <button type="button" class="btn btn-info" name="info">Edit</button></a>
                  <a href="<?php echo base_url('yonetim/yorumdelete/'.$bilgi['idn'].'/idn/comments'); ?>">
                    <button type="button" class="btn btn-danger" name="delete">Delete</button></a></td>
                </tr>
                <?php } ?>
                </tbody>
              </table>
               <p>
                  <b>Don't accept comment without reading</b>
                  If you see any wrong correct.
                </p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <script>
    function go(id, url, durum){
      $.post(url, {Id: id, durum: durum}, function(response) {});
  }
</script>
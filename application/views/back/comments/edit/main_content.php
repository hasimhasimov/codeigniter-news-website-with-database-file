<!-- Main content -->
    <section class="content">
      <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Comment form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="<?php echo base_url('yonetim/yorumguncelle'); ?>" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                <input type="hidden" name="idn" value="<?php echo $bilgi['idn']; ?>">
                <input type="hidden" name="durum" value="<?php echo $bilgi['status']; ?>">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Name</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" placeholder="News Title" name="name" value="<?php echo $bilgi['name']; ?>">
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Comment</label>

                  <div class="col-sm-10">
                    <textarea name="comment" id="editor1" rows="8" cols="80">
                      <?php echo $bilgi['comment']; ?>
                    </textarea>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Email</label>

                  <div class="col-sm-8">
                    <input type="text" name="email" disabled class="form-control" value="<?php echo $bilgi['email']; ?>">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-default"><a href="<?php echo base_url("yonetim/yorumlar"); ?>">Cancel</a></button>
                <button type="submit" class="btn btn-primary pull-right">Edit</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <div class="clearfix"></div>
    </section>
    <div class="clearfix"></div>
    <script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>
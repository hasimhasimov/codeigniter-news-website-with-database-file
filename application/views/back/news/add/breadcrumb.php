 <section class="content-header">
      <h1>
        H News
        <small>Control Panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('yonetim') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url('yonetim/newslist') ?>"><i class="fa fa-newspaper-o"></i> News</a></li>
        <li class="active">Add News</li>
      </ol>
    </section>
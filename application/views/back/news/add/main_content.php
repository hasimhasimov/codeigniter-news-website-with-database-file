<!-- Main content -->
    <section class="content">
      <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add News form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="<?php echo base_url('yonetim/addnews'); ?>" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">News Title</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" placeholder="News Title" name="title">
                  </div>
                </div>
              </div>
                <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">News Category</label>

                  <div class="col-sm-3">
                    <select class="form-control" name="cat_id">
                      <?php $bilgi=kategoriliste(); 
                      foreach ($bilgi as $bilgi) { ?>
                        <option value="<?php echo $bilgi['idn']; ?>"><?php echo $bilgi['title']; ?></option>
                        <?php }  ?>
                  </select>
                  </div>

                  <label  class="col-sm-2 control-label">News Photo</label>

                  <div class="col-sm-3">
                    <input type="file" class="form-control" name="tmb">
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Date</label>

                  <div class="col-sm-8">
                    <input type="date" name="tarix" class="form-control">
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">News Content</label>

                  <div class="col-sm-10">
                    <textarea name="content" id="editor1" rows="8" cols="80"></textarea>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">News Comment Allow</label>

                  <div class="col-sm-3">
                    <select class="form-control" name="comment">
                      <option value="1">Allow Comment</option>
                      <option value="2">No Comment</option>
                    </select>
                  </div>
                  <label  class="col-sm-2 control-label">News Headline Allow </label>

                  <div class="col-sm-3">
                    <select class="form-control" name="hot_news">
                      <option value="1">Normal News</option>
                      <option value="2">Hot News</option>
                      <option value="3">Slider News</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">News Tags</label>

                  <div class="col-sm-8">
                    <input type="text" name="tags" placeholder="Tag ex: Economy, Sport" class="form-control">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-default"><a href="<?php echo base_url("yonetim/newslist"); ?>">Cancel</a></button>
                <button type="submit" class="btn btn-primary pull-right">Add</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <div class="clearfix"></div>
    </section>
    <div class="clearfix"></div>
    <script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>
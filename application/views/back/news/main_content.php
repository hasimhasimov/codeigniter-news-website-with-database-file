<!-- Main content -->
    <section class="content">
      <div class="row">
        <?php echo $this->session->flashdata("durum"); ?>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">News List</h3>
              <a href="<?php echo base_url('yonetim/newsadd'); ?>" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>News Title</th>
                  <th>News Category</th>
                  <th>Headline Status</th>
                  <th>Comment Status</th>
                  <th>Status</th>
                  <th>Operations</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($bilgi as $bilgi){ ?>
                <tr>
                  <td><?php echo $bilgi['idn']; ?></td>
                  <td><?php echo word_limiter($bilgi['title'], 4); ?></td>
                  <td><?php echo $bilgi['cat_id']; ?></td>
                  <td>
                    <?php if ($bilgi['hot_news']==1) {?>
                    <button type="button" class="btn btn-primary" name="button">Normal News</button>
                    <?php }elseif($bilgi['hot_news']==2){ ?>
                    <button type="button" class="btn btn-warning" name="button">Hot News</button>
                    <?php }else{ ?>
                    <button type="button" class="btn btn-success" name="button">Slider News</button>
                    <?php } ?>
                  </td>
                  <td>
                    <?php if ($bilgi['comment']==1) {?>
                    <button type="button" class="btn btn-primary" name="button">Comment ON</button>
                    <?php }else{ ?>
                    <button type="button" class="btn btn-danger" name="button">Comment Off</button>
                    <?php } ?>
                  </td>
                  <td>
                    <input 
                    class="toggle_check"
                    data-toggle="toggle"
                    data-on="Active"
                    data-onstyle="success"
                    data-off="Deactive" 
                    data-offstyle="danger"
                    type="checkbox"
                    <?php echo ($bilgi['durum']==1) ? "checked" : ""; ?> 
                    onchange="go(<?php echo $bilgi['idn']; ?>, '<?php echo base_url("yonetim/haberset"); ?>','<?php echo ($bilgi['durum']==1) ? "false" : "true"; ?>'  );">
                  </td>
                  <td><a href="<?php echo base_url('yonetim/haberduzenle/'.$bilgi['idn'].''); ?>">
                    <button type="button" class="btn btn-info" name="info">Edit</button></a>
                  <a href="<?php echo base_url('yonetim/habersil/'.$bilgi['idn'].'/idn/news'); ?>">
                    <button type="button" class="btn btn-danger" name="delete">Delete</button></a></td>
                </tr>
                <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<script>
    function go(id, url, durum){
      $.post(url, {Id: id, durum: durum}, function(response) {});
  }
</script>
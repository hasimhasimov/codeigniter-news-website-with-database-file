<!-- Main content -->
    <section class="content">
      <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit News form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="<?php echo base_url('yonetim/haberguncelle'); ?>" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                <input type="hidden" name="idn" value="<?php echo $bilgi['idn']; ?>">
                <input type="hidden" name="durum" value="<?php echo $bilgi['durum']; ?>">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">News Title</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" placeholder="News Title" name="title" value="<?php echo $bilgi['title']; ?>">
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Current Image</label>
                  <div class="col-sm-3">
                    <img src="<?php echo base_url($bilgi['resim']) ?>" class="profile-user-image image-responsive" style="width: 60px;height: 60px;">
                  </div>
                </div>
                
              </div>
                <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">News Category</label>
                  <div class="col-sm-3">
                    <select class="form-control" name="cat_id">
                      <?php $bilgim=kategoriliste(); 
                      foreach ($bilgim as $bilgim) {
                        if ($bilgim['idn']==$bilgi['cat_id']) {
                       ?>
                        <option selected value="<?php echo $bilgim['idn']; ?>"><?php echo $bilgim['title']; ?></option>
                        <?php }else{ ?>
                        <option value="<?php echo $bilgim['idn']; ?>"><?php echo $bilgim['title']; ?></option>

                        <?php } }  ?>
                  </select>
                  </div>

                  <label  class="col-sm-2 control-label">News Photo</label>

                  <div class="col-sm-3">
                    <input type="file" class="form-control" name="tmb">
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Date</label>

                  <div class="col-sm-8">
                    <input type="date" name="tarix" class="form-control" value="<?php echo $bilgi['date']; ?>">
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">News Content</label>

                  <div class="col-sm-10">
                    <textarea name="content" id="editor1" rows="8" cols="80">
                      <?php echo $bilgi['content']; ?>
                    </textarea>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">News Comment Allow</label>

                  <div class="col-sm-3">
                    <select class="form-control" name="comment">
                      <?php if($bilgi['comment']==1){?>
                      <option selected value="1">Allow Comment</option>
                      <option value="2">No Comment</option>
                      <?php }else{ ?>
                      <option  value="1">Allow Comment</option>
                      <option selected value="2">No Comment</option>
                      <?php } ?>
                    </select>
                  </div>
                  <label  class="col-sm-2 control-label">News Headline Allow </label>

                  <div class="col-sm-3">
                    <select class="form-control" name="hot_news">
                      <?php if($bilgi['hot_news']==1){ ?>
                      <option selected value="1">Normal News</option>
                      <option value="2">Hot News</option>
                      <option value="3">Slider News</option>
                      <?php }elseif($bilgi['hot_news']==2){ ?>
                      <option  value="1">Normal News</option>
                      <option selected value="2">Hot News</option>
                      <option value="3">Slider News</option>
                      <?php }else{ ?>
                      <option  value="1">Normal News</option>
                      <option  value="2">Hot News</option>
                      <option value="3" selected>Slider News</option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">News Tags</label>

                  <div class="col-sm-8">
                    <input type="text" name="tags" placeholder="Tag ex: Economy, Sport" class="form-control" value="<?php echo $bilgi['tags']; ?>">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-default"><a href="<?php echo base_url("yonetim/newslist"); ?>">Cancel</a></button>
                <button type="submit" class="btn btn-primary pull-right">Edit</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <div class="clearfix"></div>
    </section>
    <div class="clearfix"></div>
    <script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>
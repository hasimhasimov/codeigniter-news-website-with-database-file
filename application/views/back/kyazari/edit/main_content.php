<!-- Main content -->
    <section class="content">
      <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Kose Yazari Duzenle</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="<?php echo base_url('yonetim/kyazariguncelle'); ?>" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Ad Soyad</label>

                  <div class="col-sm-8">
                    <input type="hidden" name="id" value="<?php echo $bilgi['idn']; ?>">
                    <input type="hidden" name="durum" value="<?php echo $bilgi['durum']; ?>">
                    <input type="text" class="form-control" value="<?php echo $bilgi['name']; ?>" name="name">
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Mevcut Resim</label>

                  <div class="col-sm-4">
                    <img src="<?php echo base_url(''); echo $bilgi['resim']; ?>" class="profile-user-img img-responsive">
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Resim</label>

                  <div class="col-sm-8">
                    <input type="file" class="form-control"  name="resim">
                  </div>
                </div>
              </div>
                
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-default"><a href="<?php echo base_url("yonetim/koseyazari"); ?>">Cancel</a></button>
                <button type="submit" class="btn btn-primary pull-right">Duzenle</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <div class="clearfix"></div>
    </section>
    <div class="clearfix"></div>
    <script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>
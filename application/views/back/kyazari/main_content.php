<!-- Main content -->
    <section class="content">
      <div class="row">
        <?php echo $this->session->flashdata("durum"); ?>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Kose Yazarlari Listesi</h3>
              <a href="<?php echo base_url('yonetim/kyazariekle'); ?>" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Yazar Isim Soyisim</th>
                  <th>Resim</th>
                  <th>Status</th>
                  <th>Operations</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($bilgi as $bilgi){ ?>
                <tr>
                  <td><?php echo $bilgi['idn']; ?></td>
                  <td><?php echo $bilgi['name']; ?></td>
                  <td><img src="<?php echo base_url(''); echo $bilgi['resim']; ?>" class="profile-user-img img-responsive"></td>
                    <td><input 
                    class="toggle_check"
                    data-toggle="toggle"
                    data-on="Active"
                    data-onstyle="success"
                    data-off="Deactive" 
                    data-offstyle="danger"
                    type="checkbox"
                    <?php echo ($bilgi['durum']==1) ? "checked" : ""; ?> 
                    onchange="go(<?php echo $bilgi['idn']; ?>, '<?php echo base_url("yonetim/yazarset"); ?>','<?php echo ($bilgi['durum']==1) ? "false" : "true"; ?>'  );">
                  </td>
                  <td><a href="<?php echo base_url('yonetim/yazarduzenle/'.$bilgi['idn'].''); ?>">
                    <button type="button" class="btn btn-info" name="info">Edit</button></a>
                  <a href="<?php echo base_url('yonetim/kyazarisil/'.$bilgi['idn'].'/idn/yazarlar'); ?>">
                    <button type="button" class="btn btn-danger" name="delete">Delete</button></a></td>
                </tr>
                <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<script>
    function go(id, url, durum){
      $.post(url, {Id: id, durum: durum}, function(response) {});
  }
</script>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <?php echo $this->session->flashdata("durum"); ?>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Category List</h3>
              <a href="<?php echo base_url('yonetim/catadd'); ?>" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Category Title</th>
                  <th>Category Seflink</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($bilgi as $bilgi){ ?>
                <tr>
                  <td><?php echo $bilgi['idn']; ?></td>
                  <td><?php echo $bilgi['title']; ?></td>
                  <td><?php echo $bilgi['selflink']; ?></td>
                </tr>
                <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
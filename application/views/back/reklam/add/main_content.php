<!-- Main content -->
    <section class="content">
      <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Reklam Ekle</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="<?php echo base_url('yonetim/reklamekleme'); ?>" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Sirket Adi</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" placeholder="sirket" name="sirket">
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Resim</label>

                  <div class="col-sm-8">
                    <input type="file" class="form-control"  name="resim">
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Baslangic Tarihi</label>

                  <div class="col-sm-8">
                    <input type="date" class="form-control" name="baslangic">
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Bitis Tarihi</label>

                  <div class="col-sm-8">
                    <input type="date" class="form-control" name="bitis">
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">URL</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="url">
                  </div>
                </div>
              </div>
                
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-default"><a href="<?php echo base_url("yonetim/reklam"); ?>">Cancel</a></button>
                <button type="submit" class="btn btn-primary pull-right">Add</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <div class="clearfix"></div>
    </section>
    <div class="clearfix"></div>
    <script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>

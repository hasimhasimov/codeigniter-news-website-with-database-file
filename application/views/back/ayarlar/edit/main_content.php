<!-- Main content -->
    <section class="content">
      <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Site Ayarlari Duzenleme</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="<?php echo base_url('yonetim/ayarguncelle'); ?>" method="POST">
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Site Title</label>

                  <div class="col-sm-6">
                    <input type="text" value="<?php echo $bilgi['title']; ?>" class="form-control" placeholder="Site Title" name="title">
                    <input type="hidden" name="idn" value="<?php echo $bilgi['idn']; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Site Mail</label>

                  <div class="col-sm-6">
                    <input type="email" value="<?php echo $bilgi['mail']; ?>" class="form-control" placeholder="Site Mail" name="site_mail">
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Site Phone</label>

                  <div class="col-sm-6">
                    <input type="phone" value="<?php echo $bilgi['phone']; ?>" class="form-control" placeholder="Site Phone" name="phone">
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Site Description</label>

                  <div class="col-sm-6">
                    <textarea name="desc" rows="8" cols="80"><?php echo $bilgi['description']; ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Site Adress</label>

                  <div class="col-sm-6">
                    <textarea name="adress" rows="8" cols="80"><?php echo $bilgi['adress']; ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Site Information</label>

                  <div class="col-sm-6">
                    <textarea id="editor1" type="text" value="<?php echo $bilgi['info']; ?>" class="form-control" placeholder="Site Info" name="info" ></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Site Keywords</label>

                  <div class="col-sm-6">
                    <input type="text" value="<?php echo $bilgi['keywords']; ?>" class="form-control" placeholder="Site Keywords" name="keywords">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-default"><a href="<?php echo base_url("yonetim/siteayarlari"); ?>">Cancel</a></button>
                <button type="submit" class="btn btn-info pull-right">Edit</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <div class="clearfix"></div>
    </section>
    <div class="clearfix"></div>
    <script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>
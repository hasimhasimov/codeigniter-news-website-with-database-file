<!-- Main content -->
    <section class="content">
      <div class="row">
        <?php echo $this->session->flashdata("durum"); ?>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Site Option List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Site Title</th>
                  <th>Site Url</th>
                  <th>Site Email</th>
                  <th>Site Phone</th>
                  <th>Operations</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($bilgi as $bilgi){ ?>
                <tr>
                  <td><?php echo $bilgi['idn']; ?></td>
                  <td><?php echo $bilgi['title']; ?></td>
                  <td><?php echo $bilgi['url']; ?></td>
                  <td><?php echo $bilgi['mail']; ?></td>
                  <td><?php echo $bilgi['phone']; ?></td>
                  <td><a href="<?php echo base_url('yonetim/ayarduzenle/'.$bilgi['idn'].''); ?>"><button type="button" class="btn btn-info" name="info">Edit</button></a></td>
                </tr>
                <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
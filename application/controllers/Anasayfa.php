<?php 

class Anasayfa extends CI_Controller{

	public function test(){
		$this->load->view('front/upload');
	}

	function do_upload(){
        $config['upload_path']=FCPATH."assets/front/images";
        $config['allowed_types']='gif|jpg|png';
        $config['encrypt_name'] = TRUE;
         
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file")){
            $data = array('upload_data' => $this->upload->data());
 
            $title= $this->input->post('title');
            $image= $data['upload_data']['file_name']; 
            $db = array('title'=>$title,'image'=>$image);
             
            $result= $this->dtbs->insert('gallery', $db);
            echo json_decode($result);
        }
 
     }

	public function index(){
		$cats = $this->dtbs->listele("category");
		$data['cats'] = $cats;
		$news = $this->dtbs->sondakika();
		$data['bnews'] = $news;
		$lastnews = $this->dtbs->listele2("news",3);
		$data['lnews'] = $lastnews;
		$mnews = $this->dtbs->listele3("news",3);
		$data['mnews'] = $mnews;
		$kyazar = $this->dtbs->listele2("yazarlar", 3000);
		$data['yazar'] = $kyazar;
		$info = $this->dtbs->fetch("site_options", 1);
		$data['info'] = $info;
		//For pagination
		$count= $this->dtbs->habersayisi();
		$this->load->library("pagination");
		$config['base_url'] = base_url().'anasayfa';
		$config['total_rows'] = $count;
		$config['per_page'] = 6;
		$config['full_tag_open'] = '<div class="pagination">';
		$config['full_tag_close'] = '</div>';
		$config['first_link'] = false;
		$config['last_link'] = false;
		$config['next_link'] = '&raquo;';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<a class="page">';
		$config['prev_tag_close'] = '</a>';
		$config['cur_tag_open'] = '<span class="current">';
		$config['cur_tag_close'] = '</span>';
		$this->pagination->initialize($config);
		$data['linkler'] = $this->pagination->create_links();
		$rnews = $this->dtbs->random("news",$config['per_page'],$this->uri->segment(2,0));
		$data['rnews'] = $rnews;
 		$this->load->view("anasayfa", $data);
	}

	public function category($selflink){
		$cats = $this->dtbs->listele("category");
		$data['cats'] = $cats;
		$news = $this->dtbs->sondakika();
		$data['bnews'] = $news;
		$lastnews = $this->dtbs->listele2("news",3);
		$data['lnews'] = $lastnews;
		$mnews = $this->dtbs->listele3("news",3);
		$data['mnews'] = $mnews;
		$kyazar = $this->dtbs->listele2("yazarlar", 3000);
		$data['yazar'] = $kyazar;
		$info = $this->dtbs->fetch("site_options", 1);
		$data['info'] = $info;
		$data['ccat'] = $this->dtbs->fetch2("category", $selflink);
		//For pagination
		$count= $this->dtbs->catnewscount("news", $selflink);
		$this->load->library("pagination");
		$config['base_url'] = base_url().'anasayfa/category/'.$selflink.'/';
		$config['total_rows'] = $count;
		$config['per_page'] = 10;
		$config['full_tag_open'] = '<div class="pagination">';
		$config['full_tag_close'] = '</div>';
		$config['first_link'] = false;
		$config['last_link'] = false;
		$config['next_link'] = '&raquo;';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<a class="page">';
		$config['prev_tag_close'] = '</a>';
		$config['cur_tag_open'] = '<span class="current">';
		$config['cur_tag_close'] = '</span>';
		$this->pagination->initialize($config);
		$data['linkler'] = $this->pagination->create_links();
		$rnews = $this->dtbs->fetchNews("news",$selflink,$config['per_page'],$this->uri->segment(4,0));
		$data['rnews'] = $rnews;
		$this->load->view('front/category/anasayfa', $data);
	}

	public function detay($selflink){
		$res = $this->dtbs->hit($selflink);
		$hit = $res['click']+1;
		$idn = $res['idn'];
		$data= array('idn'=>$idn,'click'=>$hit);
		$this->dtbs->update($data, $idn, 'idn', 'news');
		$cats = $this->dtbs->listele("category");
		$data['cats'] = $cats;
		$news = $this->dtbs->sondakika();
		$data['bnews'] = $news;
		$lastnews = $this->dtbs->listele2("news",3);
		$data['lnews'] = $lastnews;
		$mnews = $this->dtbs->listele3("news",3);
		$data['mnews'] = $mnews;
		$kyazar = $this->dtbs->listele2("yazarlar", 3000);
		$data['yazar'] = $kyazar;
		$info = $this->dtbs->fetch("site_options", 1);
		$data['info'] = $info;
		$data['ccat'] = $this->dtbs->fetch2("category", $selflink);

		$read = $this->dtbs->fetch2("news", $selflink);
		$data['read'] = $read;
		//echo $this->db->last_query();
		$this->load->view('front/detay/anasayfa', $data);
	}

	public function comments(){
		$this->form_validation->set_rules('name','name', 'trim|required|min_lenght[5]|xss_clean');
		$this->form_validation->set_rules('email','email', 'trim|required|valid_email|xss_clean');
		$this->form_validation->set_rules('comment','comment', 'trim|required|xss_clean');
		echo $this->input->post('name');
		
		$errors = array(
			'required' => '{field} yerləri doldurmağa məcbursunuz',
			'min_lenght' => 'Ad Soyad ən az 5 hərfdən ibarət olmalıdır!',
			'valid_email' =>  'Düzgün email adresizi yazın!!'
		);
		$this->form_validation->set_message($errors);
		if ($this->form_validation->run()!=FALSE) {
			echo $this->session->set_flashdata('error', '<div class="quote">
	<blockquote>
		<div class="quote-inner">
			<div class="post-inner-content">
				<p>'.$errors['valid_email'].'</p>
			</div>
		</div>
	</blockquote>
</div>');
			redirect($_SERVER['HTTP_REFERER']);
		}else{
			$data = array(
				'name' => $name=$this->input->post('name', true),
				'email'=> $email = $this->input->post('email', true),
				'comment' => $comment=$this->input->post('comment', true),
				'newsid' => $newsid = $this->input->post('newsid'),
				'ip' => $newsid = $this->input->post('ip'),
				'status' => $durum =0,
				'date' => $date = date('Y-m-d')
			);
			$result = $this->dtbs->insert('comments', $data);
			if ($result) {
			echo $this->session->set_flashdata('error', '<div class="quote">
	<blockquote>
		<div class="quote-inner">
			<div class="post-inner-content">
				<p>Təşəkkürlər</p>
				<p>Rəyiniz təsdiqləndikdə yayınlanacaqdır...</p>
			</div>
		</div>
	</blockquote>
</div>');
			redirect($_SERVER['HTTP_REFERER']);
			}else{
				echo $this->session->set_flashdata('error', '<div class="quote">
	<blockquote>
		<div class="quote-inner">
			<div class="post-inner-content">
				<p>Təəssüf</p>
				<p>Rəyiniz yazıla bilmədi...</p>
			</div>
		</div>
	</blockquote>
</div>');
			redirect($_SERVER['HTTP_REFERER']);
			}
		}
	}

	public function kunye(){
		$cats = $this->dtbs->listele("category");
		$data['cats'] = $cats;
		$news = $this->dtbs->sondakika();
		$data['bnews'] = $news;
		$lastnews = $this->dtbs->listele2("news",3);
		$data['lnews'] = $lastnews;
		$mnews = $this->dtbs->listele3("news",3);
		$data['mnews'] = $mnews;
		$kyazar = $this->dtbs->listele2("yazarlar", 3000);
		$data['yazar'] = $kyazar;
		$info = $this->dtbs->fetch("site_options", 1);
		$data['info'] = $info;
		$this->load->view('front/kunye/anasayfa', $data);
	}
	public function about(){
		$cats = $this->dtbs->listele("category");
		$data['cats'] = $cats;
		$news = $this->dtbs->sondakika();
		$data['bnews'] = $news;
		$lastnews = $this->dtbs->listele2("news",3);
		$data['lnews'] = $lastnews;
		$mnews = $this->dtbs->listele3("news",3);
		$data['mnews'] = $mnews;
		$kyazar = $this->dtbs->listele2("yazarlar", 3000);
		$data['yazar'] = $kyazar;
		$info = $this->dtbs->fetch("site_options", 1);
		$data['info'] = $info;
		$this->load->view('front/about/anasayfa', $data);
	}
	public function contact(){
		$cats = $this->dtbs->listele("category");
		$data['cats'] = $cats;
		$news = $this->dtbs->sondakika();
		$data['bnews'] = $news;
		$lastnews = $this->dtbs->listele2("news",3);
		$data['lnews'] = $lastnews;
		$mnews = $this->dtbs->listele3("news",3);
		$data['mnews'] = $mnews;
		$kyazar = $this->dtbs->listele2("yazarlar", 3000);
		$data['yazar'] = $kyazar;
		$info = $this->dtbs->fetch("site_options", 1);
		$data['info'] = $info;
		$this->load->view('front/contact/anasayfa', $data);
	}
}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Yonetim extends CI_Controller{

	function protect(){
		$giris = $this->session->userdata('giris');
		if (!$giris) {
			redirect('yonetim');
		}
	}

  function index()
  {
      $giris = $this->session->userdata('giris');
      if ($giris) {
      	redirect('yonetim/anasayfa');
      }
      $this->load->view('back/giris');
  }

  public function giris(){
  	$email = $this->input->post('email');
  	$pass = $this->input->post('pass');

  	$control = $this->dtbs->control($email, $pass);

  	if ($control) {
  		$this->session->set_userdata('giris', true);
  		redirect('yonetim/anasayfa');
  	}else{
  		$this->session->set_flashdata('durum', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Xəta!</h4>
                Şifrə və ya email bazada yoxdur. Yenidən sınayın
              </div>');
  		redirect('yonetim');
  	}
  }


  public function anasayfa(){

  	$this->protect();
    $data['count_of_news'] = get_count('news');
    $data['count_of_cat'] = get_count('category');
    $data['count_of_author'] = get_count('yazarlar');
    $data['count_of_team'] = get_count('kunye');

  	$this->load->view('back/anasayfa', $data);
  }

  public function logout(){
  	$this->session->sess_destroy();
  	redirect('yonetim');
  }

  //site Yonetim Genel Islemler Bitis

  //Kategoriler Baslangic

  public function siteayarlari(){
  	$result = $this->dtbs->listele("site_options");
    $data['bilgi'] = $result;
    $this->load->view('back/ayarlar/anasayfa', $data);

  } 

  public function ayarduzenle($id){

    $result=$this->dtbs->fetch("site_options", $id);
    $data['bilgi'] = $result;
    $this->load->view('back/ayarlar/edit/anasayfa', $data);
  }

  public function ayarguncelle(){

    $data = array(
      'idn' => $idn = $this->input->post('idn'),
      'title' => $title = $this->input->post('title'),
      'phone' => $phone = $this->input->post('phone'),
      'mail' => $mail=$this->input->post('site_mail'),
      'description' => $desc = $this->input->post('desc'),
      'adress' => $adress = $this->input->post('adress'),
      'info' => $adress = $this->input->post('info'),
      'keywords' => $adress = $this->input->post('keywords')
    );

    $result = $this->dtbs->update($data, $idn, "idn", 'site_options');
    if ($result) {
      $this->session->set_flashdata('durum', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Good Job!</h4>
                The options has been updated succesfully!
              </div>');
      redirect('yonetim/siteayarlari');
    }else{
      $this->session->set_flashdata('durum', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Good Job!</h4>
                The options can not updated!
              </div>');
      redirect('yonetim/siteayarlari');
    }
  }

  //Site option bitdi
  //Social media basladi

  public function sosialmedia(){
    $result=$this->dtbs->listele("socialmedia");
    $data['bilgi'] = $result;
    $this->load->view('back/sosialmedia/anasayfa', $data);
  }

  public function smadd(){
    $this->load->view("back/sosialmedia/add/anasayfa");
  }

  public function smedit($id){
    $result=$this->dtbs->fetch("socialmedia", $id);
    $data['bilgi'] = $result;
    $this->load->view('back/sosialmedia/edit/anasayfa', $data);
  }
  public function smguncelle(){
    $data = array(
      'idn' => $idn = $this->input->post('idn'),
      'title' => $title = $this->input->post('title'),
      'url' => $url = $this->input->post('url')
    );

    $result = $this->dtbs->update($data, $idn, "idn", 'socialmedia');
    if ($result) {
      $this->session->set_flashdata('durum', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Good Job!</h4>
                 Social Media has been updated succesfully!
              </div>');
      redirect('yonetim/sosialmedia');
    }else{
      $this->session->set_flashdata('durum', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Good Job!</h4>
                Social media can not updated!
              </div>');
      redirect('yonetim/sosialmedia');
    }
  }
  public function addsocialmedia(){
    $data=array(
      'title' => $title = $this->input->post("title"),
      'url' => $url = $this->input->post("url"),
      'durum' => 1
    );

    $result = $this->dtbs->insert("socialmedia", $data);
    if ($result) {
      $this->session->set_flashdata('durum', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Good Job!</h4>
                Data has been inserted succesfully!
              </div>');
      redirect('yonetim/sosialmedia');
    }else{
      $this->session->set_flashdata('durum', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i>Warning</h4>
                Data can not inserted! Try Again...
              </div>');
      redirect('yonetim/sosialmedia');
    }
  }
  public function smdelete($id, $where, $from){
   $result = $this->dtbs->delete($id, $where, $from);
   if ($result) {
      $this->session->set_flashdata('durum', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Good Job!</h4>
                Data has been Deleted!
              </div>');
      redirect('yonetim/sosialmedia');
    }else{
      $this->session->set_flashdata('durum', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i>Warning</h4>
                Data can not Deleted! Try Again...
              </div>');
      redirect('yonetim/sosialmedia');
    }
  }
  public function smset(){
    $Id=$this->input->post('Id');
    $durum = ($this->input->post('durum') == "true") ? 1 : 0;
    $result=$this->db->where("idn", $Id)->update('socialmedia', array('durum'=>$durum));

    if ($result) {
      echo "<script>alert('OKay');</script>";
    }else{
      echo "<script>alert('NOOOO');</script>";
    }
  }

  //Sosyal Media Bitdi

  //Categories Basladi

  public function categories(){
    $result=$this->dtbs->listele("category");
    $data['bilgi'] = $result;
    $this->load->view('back/categories/anasayfa', $data);
  }
  public function catadd(){
    $this->load->view('back/categories/add/anasayfa');
  }

  public function addcat(){
    $data=array(
      'title' => $title = $this->input->post("title"),
      'selflink' => permalink($title)
    );

    $result = $this->dtbs->insert("category", $data);
    if ($result) {
      $this->session->set_flashdata('durum', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Good Job!</h4>
                Data has been inserted succesfully!
              </div>');
      redirect('yonetim/categories');
    }else{
      $this->session->set_flashdata('durum', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i>Warning</h4>
                Data can not inserted! Try Again...
              </div>');
      redirect('yonetim/categories');
    }
  }

  //Categories Bitdi

  //News Basladi

  function newslist(){
    $result=$this->dtbs->listele("news");
    $data['bilgi'] = $result;
    $this->load->view('back/news/anasayfa', $data);
  }

  public function newsadd(){
    $this->load->view('back/news/add/anasayfa');
  }

  public function addnews(){

    $title = $this->input->post("title");
    $cat_id = $this->input->post("cat_id");
    $cat_name = "nothing";
    $selflink = permalink($title);
    $date = $this->input->post("tarix");
    
    $content = $this->input->post("content");
    $comment = $this->input->post("comment");
    $hot_news = $this->input->post("hot_news");
    $tag = $this->input->post("tags");
    $hit=0;
    $durum = 1;
    
    $config['upload_path'] = FCPATH.'assets/front/images/haber/';
    $config['allowed_types'] = 'gif|jpg|jpeg|png';
    $config['encrypt_name'] = TRUE;
    $this->load->library('upload', $config);
    if ($this->upload->do_upload('tmb')) {
          $resim = $this->upload->data();
          $resimyolu = $resim['file_name'];
          $resimkayit = 'assets/front/images/haber/'.$resimyolu.' '; 
          $resimtmb = 'assets/front/images/haber/tmb/'.$resimyolu.' '; 
          $resimmini = 'assets/front/images/haber/mini/'.$resimyolu.' '; 
          $config['image_library'] = 'gd2';
          $config['source_image'] = 'assets/front/images/haber/'.$resimyolu.' '; 
          $config['new_image'] = 'assets/front/images/haber/tmb/'.$resimyolu;
          $config['create_thumb'] = false;
          $config['maintain_ratio'] = false;
          $config['quality'] = '60%';
          $config['width'] = 409;
          $config['height'] = 260;

          $this->load->library("image_lib", $config);
          $this->image_lib->initialize($config);
          $this->image_lib->resize();
          $this->image_lib->clear();

          //Tmb Klasor Islemleri Bitdi

          //Mini Klasor Islemleri basladi
          $config1['image_library'] = 'gd2';
          $config1['source_image'] = 'assets/front/images/haber/'.$resimyolu.' '; 
          $config1['new_image'] = 'assets/front/images/haber/mini/'.$resimyolu;
          $config1['create_thumb'] = false;
          $config1['maintain_ratio'] = false;
          $config1['quality'] = '60%';
          $config1['width'] = 94;
          $config1['height'] = 73;

          $this->load->library("image_lib", $config1);
          $this->image_lib->initialize($config1);
          $this->image_lib->resize();
          $this->image_lib->clear();

          $data = array(
            'resim'=>$resimkayit,
            'tmb' => $resimtmb,
            'mini' => $resimmini,
            'title' => $title,
            'selflink' => $selflink,
            'cat_id' => $cat_id,
            'date' => $date,
            'content' => $content,
            'comment' => $comment,
            'tags' => $tag,
            'hot_news'=>$hot_news,
            'click' => 0,
            'durum' => 1
          );
          $result=$this->dtbs->insert('news', $data);
          if ($result) {
            
          $this->session->set_flashdata('durum', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i>Əməliyyat Tamamlandı!</h4>
                Xəbər Yükləndi !
              </div>');
      redirect('yonetim/newslist');
    }else{
      $this->session->set_flashdata('durum', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i>Xəta</h4>
                Xəbər Yüklənə Bilmədi! Birdaha sına...
              </div>');
      redirect('yonetim/newslist');
    }

        }else{
          $this->session->set_flashdata('durum', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i>Warning</h4>
                Şəkil Yüklənə bilmədi! Birdaha sına...
              </div>');
      redirect('yonetim/newslist');

        }     
  }

  public function haberset(){
     $id=$this->input->post('Id');
    $durum = ($this->input->post('durum') == "true") ? 1 : 0;
    $result=$this->dtbs->update(array('durum'=>$durum),$id,"idn", "news");
    echo "<script>alert('asd');</script>";
  }

  public function haberduzenle($id){
    $result=$this->dtbs->fetch("news", $id);
    $data['bilgi'] = $result;
    $this->load->view('back/news/edit/anasayfa', $data);
  }

  public function haberguncelle(){
    $idn=$this->input->post("idn");
    $durum=$this->input->post("durum");
     $title = $this->input->post("title");
    $cat_id = $this->input->post("cat_id");
    $cat_name = "nothing";
    $selflink = permalink($title);
    $date = $this->input->post("tarix");
    
    $content = $this->input->post("content");
    $comment = $this->input->post("comment");
    $hot_news = $this->input->post("hot_news");
    $tag = $this->input->post("tags");
    
    $config['upload_path'] = FCPATH.'assets/front/images/haber';
    $config['allowed_types'] = 'gif|jpg|jpeg|png';
    $config['encrypt_name'] = TRUE;
    $this->load->library('upload', $config);
    if ($this->upload->do_upload('tmb')) {
          $resim = $this->upload->data();
          $resimyolu = $resim['file_name'];
          $resimkayit = 'assets/front/images/haber/'.$resimyolu.' '; 
          $resimtmb = 'assets/front/images/haber/tmb/'.$resimyolu; 
          $resimmini = 'assets/front/images/haber/mini/'.$resimyolu; 
          $config['image_library'] = 'gd2';
          $config['source_image'] = 'assets/front/images/haber/'.$resimyolu.' '; 
          $config['new_image'] = 'assets/front/images/haber/tmb/'.$resimyolu;
          $config['create_thumb'] = false;
          $config['maintain_ratio'] = false;
          $config['quality'] = '60%';
          $config['width'] = 409;
          $config['height'] = 260;

          $this->load->library("image_lib", $config);
          $this->image_lib->initialize($config);
          $this->image_lib->resize();
          $this->image_lib->clear();

          //Tmb Klasor Islemleri Bitdi

          //Mini Klasor Islemleri basladi
          $config1['image_library'] = 'gd2';
          $config1['source_image'] = 'assets/front/images/haber/'.$resimyolu.' '; 
          $config1['new_image'] = 'assets/front/images/haber/mini/'.$resimyolu;
          $config1['create_thumb'] = false;
          $config1['maintain_ratio'] = false;
          $config1['quality'] = '60%';
          $config1['width'] = 94;
          $config1['height'] = 73;

          $this->load->library("image_lib", $config1);
          $this->image_lib->initialize($config1);
          $this->image_lib->resize();
          $this->image_lib->clear();

          $hsil = haberresim($idn);
          $hts = habertmb($idn);
          $hms = habermini($idn);
          unlink($hsil);
          unlink($hts);
          unlink($hms);

          $data = array(
            'resim'=>$resimkayit,
            'tmb' => $resimtmb,
            'mini' => $resimmini,
            'title' => $title,
            'selflink' => $selflink,
            'cat_id' => $cat_id,
            'date' => $date,
            'content' => $content,
            'comment' => $comment,
            'tags' => $tag,
            'hot_news'=>$hot_news,
            'durum' => $durum
          );
          $result=$this->dtbs->update($data, $idn, "idn", "news");
          if ($result) {
            
          $this->session->set_flashdata('durum', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Good Job!</h4>
                Data has been Updated succesfully!
              </div>');
      redirect('yonetim/newslist');
    }else{
      $this->session->set_flashdata('durum', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i>Warning</h4>
                Data can not Updated! Try Again...
              </div>');
      redirect('yonetim/newslist');
    }

        }else{
          $data = array(
            'title' => $title,
            'selflink' => $selflink,
            'cat_id' => $cat_id,
            'date' => $date,
            'content' => $content,
            'comment' => $comment,
            'tags' => $tag,
            'hot_news'=>$hot_news,
            'durum' => $durum
          );
          $result=$this->dtbs->update($data, $idn, "idn", "news");
          if ($result) {
            
          $this->session->set_flashdata('durum', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Good Job!</h4>
                Data has been Updated succesfully!
              </div>');
      redirect('yonetim/newslist');
    }else{
      $this->session->set_flashdata('durum', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i>Warning</h4>
                Data can not Updated! Try Again...
              </div>');
      redirect('yonetim/newslist');
    }
        } 
  }

  public function habersil($id, $where, $from){
    $hsil = haberresim($idn);
          $hts = habertmb($idn);
          $hms = habermini($idn);
          unlink($hsil);
          unlink($hts);
          unlink($hms);
    $result = $this->dtbs->delete($id, $where, $from);
   if ($result) {
      $this->session->set_flashdata('durum', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Good Job!</h4>
                Data has been Deleted!
              </div>');
      redirect('yonetim/newslist');
    }else{
      $this->session->set_flashdata('durum', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i>Warning</h4>
                Data can not Deleted! Try Again...
              </div>');
      redirect('yonetim/newslist');
    }
  }

  // Haberler Bitdi

  //Yazarlar Basladi

  public function koseyazari(){
    $result = $this->dtbs->listele('yazarlar');
    $data['bilgi'] = $result;
    $this->load->view("back/kyazari/anasayfa", $data);
  }

  public function kyazariekle(){
    $this->load->view("back/kyazari/add/anasayfa");
  }

  public function kyazariekleme(){

   
    
    $config['upload_path'] = FCPATH.'assets/front/images/yazarlar';
    $config['allowed_types'] = 'gif|jpg|jpeg|png';
    $config['encrypt_name'] = TRUE;
    $this->load->library('upload', $config);
    if ($this->upload->do_upload('resim')) {
          $resim = $this->upload->data();
          $resimyolu = $resim['file_name'];
          $resimkayit = 'assets/front/images/yazarlar/'.$resimyolu.' '; 
          $resimmini = 'assets/front/images/yazarlar/mini/'.$resimyolu.' '; 
          $config['image_library'] = 'gd2';
          $config['source_image'] = 'assets/front/images/yazarlar/'.$resimyolu.' '; 
          $config['new_image'] = 'assets/front/images/yazarlar/mini/'.$resimyolu.' ';
          $config['create_thumb'] = false;
          $config['maintain_ratio'] = false;
          $config['quality'] = '60%';
          $config['width'] = 409;
          $config['height'] = 260;

          $this->load->library("image_lib", $config);
          $this->image_lib->initialize($config);
          $this->image_lib->resize();
          $this->image_lib->clear();

          $data = array(
            'resim'=>$resimkayit,
            'mini' => $resimmini,
            'name' => $name =$this->input->post("name"),
            'durum' => $durum = 1
          );
          $result=$this->dtbs->insert('yazarlar', $data);
          if ($result) {
            
          $this->session->set_flashdata('durum', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Good Job!</h4>
                Data has been inserted succesfully!
              </div>');
      redirect('yonetim/koseyazari');
    }else{
      $this->session->set_flashdata('durum', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i>Warning</h4>
                Data can not inserted! Try Again...
              </div>');
      redirect('yonetim/koseyazari');
    }
        
//Resim Yuklenmezse
}else{
          $this->session->set_flashdata('durum', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i>Warning</h4>
                Image can not inserted! Try Again...
              </div>');
      redirect('yonetim/koseyazari');

        }     
  }

  public function yazarset(){
     $id=$this->input->post('Id');
    $durum = ($this->input->post('durum') == "true") ? 1 : 0;
    $result=$this->dtbs->update(array('durum'=>$durum),$id,"idn", "yazarlar");
  }

  public function yazarduzenle($id){
    $result=$this->dtbs->fetch("yazarlar", $id);
    $data['bilgi'] = $result;
    $this->load->view('back/kyazari/edit/anasayfa', $data);
  }

  public function kyazariguncelle(){
   $config['upload_path'] = FCPATH.'assets/front/images/yazarlar';
    $config['allowed_types'] = 'gif|jpg|jpeg|png';
    $config['encrypt_name'] = TRUE;
    $this->load->library('upload', $config);
    if ($this->upload->do_upload('resim')) {
          $resim = $this->upload->data();
          $resimyolu = $resim['file_name'];
          $resimkayit = 'assets/front/images/yazarlar/'.$resimyolu.' '; 
          $resimmini = 'assets/front/images/yazarlar/mini/'.$resimyolu.' '; 
          $config['image_library'] = 'gd2';
          $config['source_image'] = 'assets/front/images/yazarlar/'.$resimyolu.' '; 
          $config['new_image'] = 'assets/front/images/yazarlar/mini/'.$resimyolu.' ';
          $config['create_thumb'] = false;
          $config['maintain_ratio'] = false;
          $config['quality'] = '60%';
          $config['width'] = 409;
          $config['height'] = 260;

          $this->load->library("image_lib", $config);
          $this->image_lib->initialize($config);
          $this->image_lib->resize();
          $this->image_lib->clear();

          $data = array(
            'resim'=>$resimkayit,
            'mini' => $resimmini,
            'name' => $name =$this->input->post("name"),
            'durum' => $durum = 1
          );
          $result=$this->dtbs->update($data, $id, "idn", 'yazarlar');
          if ($result) {
            
          $this->session->set_flashdata('durum', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Good Job!</h4>
                Data has been Updated succesfully!
              </div>');
      redirect('yonetim/koseyazari');
    }else{
      $this->session->set_flashdata('durum', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i>Warning</h4>
                Data can not Updated! Try Again...
              </div>');
      redirect('yonetim/koseyazari');
    }
        
//Resim Yuklenmezse
}else{
  $data = array(
            'name'=>$name = $this->input->post('name')
          );
          $result=$this->dtbs->update($data, $idn, "idn", "yazarlar");
          if ($result) {
            
          $this->session->set_flashdata('durum', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Good Job!</h4>
                Data has been Updated succesfully!
              </div>');
      redirect('yonetim/koseyazari');
    }else{
      $this->session->set_flashdata('durum', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i>Warning</h4>
                Data can not Updated! Try Again...
              </div>');
      redirect('yonetim/koseyazari');
    }

      }      
  }

  public function kyazarisil($id, $where, $from){
    $result = $this->dtbs->delete($id, $where, $from);
   if ($result) {
      $this->session->set_flashdata('durum', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Good Job!</h4>
                Data has been Deleted!
              </div>');
      redirect('yonetim/koseyazari');
    }else{
      $this->session->set_flashdata('durum', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i>Warning</h4>
                Data can not Deleted! Try Again...
              </div>');
      redirect('yonetim/koseyazari');
    }
  }

  //Kose Yazarlari Bitdi

  //Kunye basladi

  public function kunye(){
    $result= $this->dtbs->listele('kunye');
    $data['bilgi'] = $result;
    $this->load->view("back/kunye/anasayfa", $data);
  }

  public function kunyeekle(){
    $this->load->view("back/kunye/add/anasayfa");
  }

  public function kunyeekleme(){
    $data=array(
      'name' => $name=$this->input->post("name"),
      'gorev' => $gorev=$this->input->post('gorev'),
      'status' => $durum=1
    );

    $result=$this->dtbs->insert("kunye", $data);
    if ($result) {
      $this->session->set_flashdata('durum', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Good Job!</h4>
                Data has been Inserted!
              </div>');
      redirect('yonetim/kunye');
    }else{
      $this->session->set_flashdata('durum', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i>Warning</h4>
                Data can not Inserted! Try Again...
              </div>');
      redirect('yonetim/kunye');
    }
  }

  public function kunyeset(){
    $id=$this->input->post('Id');
    $durum = ($this->input->post('durum') == "true") ? 1 : 0;
    $result=$this->dtbs->update(array('status'=>$durum),$id,"idn", "kunye");
  }

  public function kunyesil($id, $where, $from){
    $result = $this->dtbs->delete($id, $where, $from);
   if ($result) {
      $this->session->set_flashdata('durum', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Good Job!</h4>
                Data has been Deleted!
              </div>');
      redirect('yonetim/kunye');
    }else{
      $this->session->set_flashdata('durum', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i>Warning</h4>
                Data can not Deleted! Try Again...
              </div>');
      redirect('yonetim/kunye');
    }
  }

  //Kunye Bitdi
   // Reklam Basladi

  public function reklam(){
    $result=$this->dtbs->listele("reklam");
    $data['bilgi'] = $result;
    $this->load->view('back/reklam/anasayfa', $data);
  }

  public function reklamekle(){
    $this->load->view('back/reklam/add/anasayfa');
  }

  public function reklamekleme(){

    $config['upload_path'] = FCPATH.'assets/front/images/reklam';
    $config['allowed_types'] = 'gif|jpg|jpeg|png';
    $config['encrypt_name'] = TRUE;
    $this->load->library('upload', $config);
    if ($this->upload->do_upload('resim')) {
          $resim = $this->upload->data();
          $resimyolu = $resim['file_name'];
          $resimkayit = 'assets/front/images/reklam/'.$resimyolu.' '; 
          $resimmini = 'assets/front/images/reklam/mini/'.$resimyolu.' '; 
          $config['image_library'] = 'gd2';
          $config['source_image'] = 'assets/front/images/reklam/'.$resimyolu.' '; 
          $config['new_image'] = 'assets/front/images/reklam/tmb/'.$resimyolu.' ';
          $config['create_thumb'] = false;
          $config['maintain_ratio'] = false;
          $config['quality'] = '60%';
          $config['width'] = 729;
          $config['height'] = 90;

          $this->load->library("image_lib", $config);
          $this->image_lib->initialize($config);
          $this->image_lib->resize();
          $this->image_lib->clear();

          $data = array(
            'resim'=>$resimkayit,
            'tmb' => $resimmini,
            'sirket' => $sirket =$this->input->post("sirket"),
            'link' => $url = $this->input->post('url'),
            'baslangic' => $baslangic = date("Y-m-d"),
            'bitis' => $bitis = $bitis = date("Y-m-d"),
            'durum' => $durum = 1
          );
          $result=$this->dtbs->insert('reklam', $data);
          if ($result) {
            
          $this->session->set_flashdata('durum', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Good Job!</h4>
                Data has been inserted succesfully!
              </div>');
      redirect('yonetim/reklam');
    }else{
      $this->session->set_flashdata('durum', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i>Warning</h4>
                Data can not inserted! Try Again...
              </div>');
      redirect('yonetim/reklam');
    }
        
//Resim Yuklenmezse
}else{
          $this->session->set_flashdata('durum', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i>Warning</h4>
                Image can not inserted! Try Again...
              </div>');
      redirect('yonetim/koseyazari');

        }
  }

  public function reklamset(){
    $id=$this->input->post('Id');
    $durum = ($this->input->post('durum') == "true") ? 1 : 0;
    $result=$this->dtbs->update(array('durum'=>$durum),$id,"idn", "reklam");
  }


  //Yorumlar basladi---------------------------------------------------
  public function yorumlar(){
    $result=$this->dtbs->listele("comments");
    $data['bilgi'] = $result;
    $this->load->view('back/comments/anasayfa', $data);
  }
  public function yorumset(){
   $id=$this->input->post('Id');
    $durum = ($this->input->post('durum') == "true") ? 1 : 0;
    $result=$this->dtbs->update(array('status'=>$durum),$id,"idn", "comments");
    echo "<script>alert('asd');</script>";
  }
  public function yorumedit($id){
    $result=$this->dtbs->fetch("comments", $id);
    $data['bilgi'] = $result;
    $this->load->view('back/comments/edit/anasayfa', $data);
  }

  public function yorumguncelle(){
    $data = array(
      'idn' => $idn = $this->input->post('idn'),
      'name' => $title = $this->input->post('name'),
      'comment' => $url = $this->input->post('comment')
    );

    $result = $this->dtbs->update($data, $idn, "idn", 'comments');
    if ($result) {
      $this->session->set_flashdata('durum', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Good Job!</h4>
                 Comments has been updated succesfully!
              </div>');
      redirect('yonetim/yorumlar');
    }else{
      $this->session->set_flashdata('durum', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Good Job!</h4>
                Comments can not updated!
              </div>');
      redirect('yonetim/yorumlar');
    }
  }
  public function yorumdelete($id, $where, $from){
   $result = $this->dtbs->delete($id, $where, $from);
   if ($result) {
      $this->session->set_flashdata('durum', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Good Job!</h4>
                Data has been Deleted!
              </div>');
      redirect('yonetim/yorumlar');
    }else{
      $this->session->set_flashdata('durum', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i>Warning</h4>
                Data can not Deleted! Try Again...
              </div>');
      redirect('yonetim/yorumlar');
    } 
  }
}
?>

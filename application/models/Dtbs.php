<?php 
 
 /**
 * 
 */
 class Dtbs extends CI_Model
 {
 	function control($email, $sifre){
 		$result = $this->db->select('*')
 		->from('admin')
 		->where('email', $email)
 		->where('pass', sha1(md5($sifre)))
 		->get()->row();
 		return $result;
 	}

 	function listele($from){
 		$result = $this->db
 		->select('*')
 		->from($from)
 		->order_by('idn', 'desc')
 		->get()->result_array();
 		return $result;
 	}
 	function listele2($from, $limit){
 		$result = $this->db
 		->select('*')
 		->from($from)
 		->where("durum", 1)
 		->order_by('idn', 'desc')
 		->limit($limit)
 		->get()->result_array();
 		return $result;
 	}
    function listele3($from, $limit){
        $result = $this->db
        ->select('*')
        ->from($from)
        ->where("durum", 1)
        ->order_by('click', 'desc')
        ->limit($limit)
        ->get()->result_array();
        return $result;
    }
    function random($from, $per, $set){
        $result = $this->db
        ->select('*')
        ->from($from)
        ->where("durum", 1)
        ->order_by('idn' ,'desc')
        ->limit($per, $set)
        ->get()->result_array();
        return $result;
    }

 	function fetch($from, $idn){
 		$result=$this->db
 		->select("*")
 		->from($from)
 		->where("idn", $idn)
 		->get()->row_array();
 		return $result;
 	}
    function fetch2($from, $idn){
        $result=$this->db
        ->select("*")
        ->from($from)
        ->where("selflink", $idn)
        ->get()->row_array();
        return $result;
    }

 	function update($data = array(), $id, $where, $from){
 		$result=$this->db
 		->where($where, $id)
 		->update($from,$data);
 		return $result;
 	}
    function fetchNews($from, $selflink, $per, $set){
        $idn=$this->db->select("idn")
        ->from("category")
        ->where("selflink", $selflink)->get()->row_array();

        $result = $this->db
        ->select('*')
        ->from($from)
        ->where("durum", 1)
        ->where("cat_id", $idn['idn'])
        ->order_by('idn' ,'desc')
        ->limit($per, $set)
        ->get()->result_array();
        return $result;
    }


 	function insert($from, $data = array()){
 		$result = $this->db
 		->insert($from, $data);
 		return $result;
 	}

 	function delete($id, $where, $from){
 		$result = $this->db
 		->delete($from, array($where=>$id));
 		return $result;
 	}
 	function habercek($nkat){
 	$result = $this->db->select("*")
    ->from('news')
    ->where('cat_id', $nkat)
    ->where("durum", '1')
    ->order_by("idn", "desc")
    ->limit('4', '1')->get()->result_array();
    return $result;
 	}
 	function sondakika(){
 	$result = $this->db->select("*")
    ->from('news')
    ->where('hot_news', 2)
    ->where("durum", '1')
    ->order_by("idn", "desc")
    ->limit('4', '1')->get()->result_array();
    return $result;
 	}

    //for pagination

    function habersayisi(){
        $result = $this->db->select('*')
        ->from("news")->where("durum", '1')
        ->count_all_results();
        return $result;
    }
    function catnewscount($from, $selflink){
        $idn=$this->db->select("idn")
        ->from("category")
        ->where("selflink", $selflink)->get()->row_array();

        $result=$this->db
        ->select("*")
        ->from($from)
        ->where("cat_id", $idn['idn'])
        ->where("durum", 1)
        ->count_all_results();
        return $result;
    }

    function hit($selflink){
        $result = $this->db->select('*')
        ->from('news')
        ->where('durum', '1')
        ->where('selflink', $selflink)
        ->get()->row_array();
        return $result;
    }
    



 	
 }

?>